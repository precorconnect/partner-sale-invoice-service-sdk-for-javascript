import {inject} from 'aurelia-dependency-injection';
import PartnerSaleInvoiceServiceSdkConfig from './partnerSaleInvoiceServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import PartnerSaleInvoiceView from './partnerSaleInvoiceView';

@inject(PartnerSaleInvoiceServiceSdkConfig, HttpClient)
class AddPartnerSaleRegIdToPartnerSaleInvoiceFeature {

    _config:PartnerSaleInvoiceServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }


    execute(id:string,
            request:number,
            accessToken:string){

        return this._httpClient
            .createRequest(`partner-sale-invoices/${id}/partner-sale-registration-id`)
            .asPost()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .withContent(request)
            .send()

    }
}

export default AddPartnerSaleRegIdToPartnerSaleInvoiceFeature;