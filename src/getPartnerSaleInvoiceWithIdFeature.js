import {inject} from 'aurelia-dependency-injection';
import PartnerSaleInvoiceServiceSdkConfig from './partnerSaleInvoiceServiceSdkConfig';
import {HttpClient} from 'aurelia-http-client';
import PartnerSaleInvoiceView from './partnerSaleInvoiceView';

@inject(PartnerSaleInvoiceServiceSdkConfig, HttpClient)
class GetPartnerSaleInvoiceWithIdFeature {

    _config:PartnerSaleInvoiceServiceSdkConfig;

    _httpClient:HttpClient;

    constructor(config,
                httpClient) {

        if (!config) {
            throw 'config required';
        }
        this._config = config;

        if (!httpClient) {
            throw 'httpClient required';
        }
        this._httpClient = httpClient;

    }

    /**
     * Gets the partner sale invoice with the provided id.
     * @param {string} id
     * @param accessToken
     * @returns {Promise.<PartnerSaleInvoiceView>}
     */
    execute(id:string,
            accessToken:string):Promise<PartnerSaleInvoiceView> {

        return this._httpClient
            .createRequest(`partner-sale-invoices/${id}`)
            .asGet()
            .withBaseUrl(this._config.precorConnectApiBaseUrl)
            .withHeader('Authorization', `Bearer ${accessToken}`)
            .send()
            .then(response =>
                new PartnerSaleInvoiceView(
                    response.content.id,
                    response.content.number,
                    response.content.fileUrl,
                    response.content.partnerSaleRegistrationId
                )
            );
    }
}

export default GetPartnerSaleInvoiceWithIdFeature;