import {Container} from 'aurelia-dependency-injection';
import {HttpClient} from 'aurelia-http-client';
import PartnerSaleInvoiceServiceSdkConfig from './partnerSaleInvoiceServiceSdkConfig';
import AddPartnerSaleInvoiceFeature from './addPartnerSaleInvoiceFeature';
import GetPartnerSaleInvoiceWithIdFeature from './getPartnerSaleInvoiceWithIdFeature';
import AddPartnerSaleRegIdToPartnerSaleInvoiceFeature from './addPartnerSaleRegIdToPartnerSaleInvoiceFeature';


/**
 * @class {DiContainer}
 */
export default class DiContainer {

    _container:Container;

    /**
     * @param {PartnerSaleInvoiceServiceSdkConfig} config
     */
    constructor(config:PartnerSaleInvoiceServiceSdkConfig) {

        if (!config) {
            throw 'config required';
        }

        this._container = new Container();

        this._container.registerInstance(PartnerSaleInvoiceServiceSdkConfig, config);
        this._container.autoRegister(HttpClient);

        this._registerFeatures();

    }

    /**
     * Resolves a single instance based on the provided key.
     * @param key The key that identifies the object to resolve.
     * @return Returns the resolved instance.
     */
    get(key:any):any {
        return this._container.get(key);
    }

    _registerFeatures() {

        this._container.autoRegister(AddPartnerSaleInvoiceFeature);
        this._container.autoRegister(GetPartnerSaleInvoiceWithIdFeature);
        this._container.autoRegister(AddPartnerSaleRegIdToPartnerSaleInvoiceFeature);

    }

}
